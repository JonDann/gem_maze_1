import random
from PIL import Image, ImageDraw

# image = Image.open('test.jpg')
# draw = ImageDraw.Draw(image)
# width = image.size[0]
# height = image.size[1]
n = 800

def gen_maze():
    maze = [[0] * n for _ in range(n)]
    new_i = 0
    new_j = 0
    i = random.randrange(2,n,2)
    j = random.randrange(2,n,2)
    maze[i][j] = 1
    print(i, j)

    def directions():
        dir = ['up', 'down', 'left', 'right']
        return random.choice(dir)

    for k in range(5000):

        d = directions()
        # print(d)

        if d == 'up':
            new_i = i - 2
            # ij = maze[new_i][j]
            if new_i%2==0 and new_i!=0 and new_i < n:
                maze[new_i][j] = 1
                maze[new_i+1][j] = 1
                i = new_i
            pass

        elif d == 'down':
            new_i = i + 2
            ij = maze[new_i][j]
            if new_i % 2 == 0 and new_i!=0 and new_i < n and ij != 1:
                maze[new_i][j] = 1
                maze[new_i-1][j] = 1
                i = new_i
            pass

        elif d == 'right':
            new_j = j + 2
            ij = maze[i][new_j]
            if new_j % 2 == 0 and new_j!=0 and new_j < n and ij != 1:
                maze[i][new_j] = 1
                maze[i][new_j-1] = 1
                j = new_j
            pass

        elif d == 'left':
            new_j = j - 2
            ij = maze[i][new_j]
            if new_j % 2 == 0 and new_j!=0 and new_j < n :
                maze[i][new_j] = 1
                maze[i][new_j+1] = 1
                j = new_j
            pass


    # for i in maze:
    #     print(i)
    return maze

maze = gen_maze()

# for x in range(width):
#     for y in range(height):
#         ij = maze[x][y]
#         if ij == 1:
#             r = 0
#             g = 0
#             b = 0
#             draw.point((x, y), (r, g, b))
#
# image.show()